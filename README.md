# Open Source Serverless Frameworks comparisons from a beginner's prespective

Function as a service (FaaS) is a cloud computing services category that offers a framework that enables clients to create, run, and manage application functionality without the difficulty of constructing and managing the infrastructure usually associated with the creation and launch of an app.One way to achieve a "serverless" architecture is to construct an application using this model, and it is usually used when designing applications for microservices. There are many open source serverless frameworks available online, to know which one to use one would have to spend many hours just researching the various aspects of each different framework.This research project offers a comparison of various available at the time of writing open source serverless frameworks from a qualitative metrics view and from the prespective of a researcher new in the whole faas IOT area trying to deploy applications using those computing methods on the cloud.

## INFO

Please note that these files are provided as an example of what pieces of code (often taken from tutorials and guides for setting up and running the open source serverless frameworks as described in the project report) were used to "test" the difficulty of setting up a sample code running on the cloud. 



## Authors

* **Nikolaos Papadakis csdp1165**

## Acknowledgments

* Hat tip to all the guides of setting up the various open source frameworks and getting the code included here to work serverlessly on the cloud.








* etc
